
#include <iostream>

using namespace std;
	
	void leer (int a[][5]);
	void visualizar (const int a[][5]);
	
	
	
	int main()
{
	int a[3][5];
	leer(a);
	visualizar(a);		
}

void leer (int a[][5])
{
cout<<"llenar una tabla de 3x5 elementos "<<endl;
for (int i=0;i<3;i++){
	cout<<"Fila "<<i<<":";
	for(int j=0;j<5;j++)
		cin>>a[i][j];
}	
}

void visualizar (const int a[][5])
{
	for (int i=0;i<3;i++){
		for (int j=0;j<5;j++)
		cout<<" "<<a[i][j];
		cout<<endl;
	}
}
