/**
 * @file Condicionales.cpp
 * @author Elvis Chambi (elvisrildo123@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 12.02.2022
 * 
 * && y
 * || o 
 * == igual
 * != diferente
 *
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void ShowResults();
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

int first;
int second;
int third;
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	ShowResults();
}
//=====================================================================================================
void CollectData(){
	
	cout<<"============Insert data============\r\n";
	cout<<"Ingrese 3 numeros diferentes"<<endl;

	cout<<"Ingrese el primer numero: "<<endl;
	cin>>first;


	cout<<"Ingrese el segundo numero:"<<endl;
	cin>>second;

	cout<<"Ingrese el tercer numero:"<<endl;
	cin>>third;
}
//=====================================================================================================	
void ShowResults(){
	if ((first> second) &&(first>third)&&(second>third)){
		cout<<"Los numeros ingresados de mayor a menor son: "<<endl;
		cout<<"~ "<<first<<endl;
		cout<<"~ " <<second<<endl;
		cout<<"~ "<<third<<endl;
	}
	if ((first>second)&&(first>third)&&(third>second)){
		cout<<"Los numeros ingresados de mayor a menor son: "<<endl;
		cout<<"~ " <<first<<endl;
		cout<<"~ "<<third<<endl;
		cout<<"~ "<<second<<endl;	
	}
	if ((second>first)&&(second>third)&&(first>third)){
		cout<<"Los numeros ingresados de mayor a menor son: "<<endl;
		cout<<"~ " <<second<<endl;
		cout<<"~ "<<first<<endl;
		cout<<"~ "<<third<<endl;	
	}	
	if ((second>first)&&(second>third)&&(third>first)){
		cout<<"Los numeros ingresados de mayor a menor son: "<<endl;
		cout<<"~ " <<second<<endl;
		cout<<"~ "<<third<<endl;
		cout<<"~ "<<first<<endl;	
	}	
	if ((third>first)&&(third>second)&&(first>second)){
		cout<<"Los numeros ingresados de mayor a menor son: "<<endl;
		cout<<"~ "<<third<<endl;
		cout<<"~ " <<first<<endl;
		cout<<"~ "<<second<<endl;	
	}	
	if ((third>first)&&(third>second)&&(second>first)){
		cout<<"Los numeros ingresados de mayor a menor son: "<<endl;
		cout<<"~ "<<third<<endl;
		cout<<"~ "<<second<<endl;
		cout<<"~ "<< first<<endl;
	}	
	}


