/**
 * @file Exercise 12
 * @author Cuevas Espinoza Rafael Edu 
 * @brief Modifique el programa anterior del tonel para que, suponiendo que las medidas de entrada son dadas en cent�metros, el resultado lo muestre en:
  litros, cent�metros c�bicos y metros c�bicos. Recuerde que 1 litro es equivalente a un dec�metro c�bico. Indique siempre la unidad de medida empleada.

 * @date 27.01.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/

#define PI 	3.1416	//Constante PI


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	//Declaration and initialization
	float height         = 0.0; //Altura del tonel
	float largerDiameter = 0.0; //Diametro mayor del centro del tonel
	float minorDiamater  = 0.0; //Diametro menor de sus extremos del tonel
	float volume         = 0.0; //volumen = capacidad del tonel en centimetros cubicos
	float volumeLiter    = 0.0; //volumen en litro
	float volumeMetro    = 0.0; //vlumen en metros cubicos 
	
	//Display phrase 1
	cout<<"========== Insert Data ==========\r\n" ;
	cout <<"Digite la altura en cm : "; 
	cin>>height ;                                      //leer la altura
	cout<<"\r\nDigite el diametro  mayor en cm : ";
	cin>>largerDiameter ;                              //leer el diametro mayor
	cout<<"\r\nDigite el diametro menor en cm : " ;
	cin>>minorDiamater ;                               //leer el diametro menor
	
	
	//Calculation
	volume = (PI/12)*(height)*(2*(pow(largerDiameter,2))+pow(minorDiamater,2)) ; //Formula para hallar el volumen del tonel con la altura y los diametros de sus extremos y  del centro
	volumeLiter = volume/1000                                                  ;  //Conversion del volumen de  centimetros cubicos a litro
	volumeMetro = volume/1000000                                               ;  //Conversion del volumen de centimetros cubicos a metros cubicos
	
	//results
	cout<<"\r\n========== Show results ==========\r\n";
	cout<<"El volumen en litros : "<<volumeLiter<<"L" ;                     //Resultado de la capacidad del tonel en litros
	cout<<"\r\nEl volumen en centimetros cubicos : "<<volume <<"cm�";       //Resultado de la capacidad del tonel en centimetros cubicos    
	cout<<"\r\nEl volumen en metros cubicos : "<<volumeMetro<<"m� " ;       //Resultado de la capacidad del tonel en metros cubicos 
	
	return 0 ;
	
}
	
