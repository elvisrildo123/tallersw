/**
 * @file codeReview.cpp
 * @author Elvis Chambi (elvisrildo123@gmail.com)
 * @brief Algorithm for calculate the volume of a tonel
 * @version 1.0
 * @date 05.12.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/



/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/



int main(){
	
	//Declaration of the variables
	
	float d; //Minor diameter of the tonel
	
	float D; //Mayor diameter of the tonel 
	
	float a; // a number that is part of V
	
	float l; //Height of the tonel
	
	float Vc; //Volume of the tonel in cubics centimeters
	
	float Vm; //Volume of the tonel in cubics meters
	
	float Vl; //Volume of the tonel in liters
	
	//Initialize of the variable
	
	d=0.0;
	
	D=0.0;
	
	a=0.0;
	
	l=0.0;
	
	Vc=0.0;
	
	Vm=0.0;
	
	Vl=0.0;
	
	//Display phrase 1
	
	cout<<"Enter the minor diameter, in centimeters, of the tonel: ";
	
	cin>>d;
	
	cout<<"Enter the mayor diameter, in centimeters, of the tonel: ";
	
	cin>>D;
	
	cout<<"Enter the height, in centimeters, of the tonel: ";
	
	cin>>l;
	
	//Operations
	
	a=d/2+(D/2+d/2)*2/3;
	
	Vc=l*a*2*3.14;
	
	Vm=Vc/1000000;
	
	Vl=Vc/1000000000;
	
	// Display phrase 2
	
	cout<<"\r\nThe volume of the tonel in cubics centimeters is: "<<Vc;
	
	cout<<"\r\nThe volume of the tonel in cubics meters is: "<<Vm;
	
	cout<<"\r\nThe volume of the tonel in liters is: "<<Vl;
	
	return 0;
}

