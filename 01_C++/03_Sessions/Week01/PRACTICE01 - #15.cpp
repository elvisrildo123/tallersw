/*Haga que el programa tambi�n calcule el punto medio de la l�nea que une los dos puntos,
 el cual viene dado por (x1+x2)/2,(y1+y2)/2. �Cu�l es el resultado que devuelve el 
 programa para los puntos (3,7) y (8,12)? */
 
  #include <iostream>
 
 
  using namespace std;
 int main(){
 	
 	//Declaration y initialize
	double X1, Y1, X2 ,Y2, medio, pendien = 0;
	
	//Display phrase 1
	cout<<"Ingrese la coordenada en X del punto 1: " <<endl;
	cin>> X1;
	
	cout<<"Ingrese la coordenada en Y del punto 1: " <<endl;
	cin>> Y1;
	
	cout<<"Ingrese la coordenada en X del punto 2: " <<endl;
	cin>> X2;
	
	cout<<"Ingrese la coordenada en Y del punto 2: " <<endl;
	cin>> Y2;
 	
 	//Operation
 	pendien = (Y2-Y1)/(X2-X1);
 	medio = (X1+X2)/2,(Y1+Y2)/2;
 	
	//Display phrase 2
	cout<<"La pendiente de la linea es: "<<pendien<<endl;
	cout<<"El punto medio de la linea es: "<<medio; 	
 	
	 return 0;
 }
