/**
 * @file Exercise07.cpp
 * @author 	Elvis Chambi (elvisrildo123@gmail.com)
 * @brief 	Una compa��a de alquiler de autos emite la factura de sus clientes teniendo en cuenta la distancia recorrida,
 			si la distancia no rebasa los 300 km., se cobra una tarifa fija de S/.250, si la distancia recorrida es mayor a 300 km. y hasta 1000 km. 
			se cobra la tarifa fija m�s el exceso de kil�metros a raz�n de S/.30 por km. y, si la distancia recorrida es mayor a 1000 km.,
			la compa��a cobra la tarifa fija m�s los kms. recorridos entre 300 y 1000 a raz�n de S/. 30, y S/.20 para las distancias mayores de 1000 km.
			Calcular el monto que pagar� un cliente.
 * @version 2.0
 * @date 28.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define TAR 250.0								// Tarifa fija

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalTerminalDistance = 0.0;				// Entrada de la distancia recorrida a trav�s del terminal
float globalMaxDistance1 = 300.0;				// Distancia m�xima 1
float globalMaxDistance2 = 1000.0;				// Distancia m�xima 1

float paymentAmount = 0.0;						// Total a pagar por el cliente

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
int CheckData();
void Calculate();
void ShowResults();
float CalculateDistance1(float distance);
float CalculateDistance2(float distance);
float CalculateDistance3(float distance);
float TotalPayment(float distance);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while(true){
		
		if (CollectData() == Error){
			cout<<"\n\t! CollectData Error !\n";
            cout<<"\n\tPlease write a correct data.\n\n";
            continue;
		}
		Calculate();
		ShowResults();
		break;
	}
	
	return true;
}

//=====================================================================================================
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the distance traveled: ";
	globalTerminalDistance = CheckData();
	if(globalTerminalDistance == -1){
		return Error;
	}
	
	return Success;
}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return -1;
			break;	
        } else { 
			if (data > 0){ 
			    return data;
				break;
			} else {
				return -1;
			}	
		}
    }
}

//=====================================================================================================
void Calculate(){
	paymentAmount = TotalPayment(globalTerminalDistance);
}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tTotal to be paid by the customer: "<<paymentAmount<<"\r\n";
}

//=====================================================================================================
float TotalPayment(float distance){
	
	float payment = 0;
	
	if (distance <= globalMaxDistance1){
		payment = CalculateDistance1(distance);
	} else {
		if (distance <= globalMaxDistance2){
			payment = CalculateDistance2(distance);
		} else {
			payment = CalculateDistance3(distance);
		}
	}

	return payment;
}

//=====================================================================================================
float CalculateDistance1(float distance){
	
	return TAR;
}

//=====================================================================================================
float CalculateDistance2(float distance){
	
	return TAR + 30 * (distance - globalMaxDistance1);
}

//=====================================================================================================
float CalculateDistance3(float distance){
	
	return TAR + 700 * 30 + 20 *(distance - globalMaxDistance2); 
}

//=====================================================================================================
