/**
 * @file Condicionales.cpp
 * @author Elvis Chambi (elvisrildo123@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 12.02.2022
 * 
 * && y
 * || o 
 * == igual
 * != diferente
 *
 */


#include <iostream>
#include <math.h>

using namespace std;

float L1=0.0;
float L2=0.0;
float L3=0.0;
float A=0.0;


//Function declaration

void Run();
void CollectData();
void Calculate();
void ShowResults();
float Area( float x1, float x2, float x3);
void KindOfTriangle();

int main(){
	
Run();
return 0;

}

void Run(){
	
	CollectData();
	Calculate();
	ShowResults();
	
}


void CollectData(){
	
	cout<<"=========Insert data=========\r\n";
	cout<<"Tipo y area de un triangulo\r\n";
	cout<<"Enter the value of the first side: ";
	cin>>L1;
	cout<<"Enter the value of the second side: ";
	cin>>L2;
	cout<<"Enter the value of the three side: ";
	cin>>L3;
	
}
	
void Calculate(){
	
	A=Area(L1, L2, L3);	
}	
	
void ShowResults(){
	
	cout<<"==========Show results==========\r\n";
	cout<<"The area is: "<<A<<endl;
	cout<<"The kind of triangle is: ";
	KindOfTriangle();

	
}	

void KindOfTriangle(){
	
	if((L1!=L2) && (L1!=L3) && (L3!=L2)){
		
		cout<<"EL triangulo es escaleno";
	}
	
	else{
		
		if((L1==L2) && (L1!=L3)){
			
			cout<<"El triangulo es isoceles";
			
		}
	    
		if((L1==L3) && (L1!=L2)){
				
			cout<<"El triangulo es isoceles";
		}
			
		if((L2==L3) && (L2!=L1)){
				
			cout<<"El triangulo es isoceles";
			
		}
		
	    if((L1==L2) && (L2==L3)){
			
			cout<<"El triangulo es equilatero";
			
		}
	}
	
}

float Area( float x1, float x2, float x3){
	
	
	int S=0.0;
	
	S=(x1+x2+x3)/2;
	
	return sqrt(S*(S-x1)*(S-x2)*(S-x3));
	
}



























