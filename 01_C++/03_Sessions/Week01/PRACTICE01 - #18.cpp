#include<iostream>
#include <math.h>  //BIBLIOTECA DE LAS FUNCIONES TRIGONOMETRICAS

/*Si coloca una escalera de 3 metros a un �ngulo de 85 grados al 
lado de un edificio, la altura en la cual la escalera toca el edificio
 se puede calcular como altura=3 * seno 85�. Calcule esta altura con una
 calculadora y luego escriba un programa en C que obtenga y visualice el
  valor de la altura.*/

using namespace std;
int main(){
	
	//Declaration
	int radian;
	double altura;
	double result;
	double PI = 3.14159;
	
	//Initialize
	radian=0;
	result=0;
	altura=0;
	
	//Display phrase 1
	cout<<"Escriba un numero para el angulo:";
	cin>>radian;
	
	//Operation
	result = sin(radian*PI/180);  //SE DA LA CONVERSION DEL NUMERO A RADIANES
	altura = 3*result;
	
	//Display phrase 2
	cout<<"La Altura en la cual la escalera toca el edificio es:"<<altura;
		
return 0;	
}

