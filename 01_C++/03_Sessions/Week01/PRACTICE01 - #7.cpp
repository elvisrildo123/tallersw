#include<iostream>
#include<math.h>

using namespace std;
int main(){
	double G;
	G=0.00000006673;
	double F;
	double M1;
	double M2;
	double D;
	double d;
	
	F = 0;
	d=0;
	
	cout<<"Ingrese el valor de la masa del cuerpo 1 (en Kg) : ";
	cin>>M1;
	cout<<"Ingrese el valor de la masa del cuerpo 2 (en Kg) : ";
	cin>>M2;
	cout<<"Ingrese el valor de la distancia entre los cuerpos (en m) : ";
	cin>>D;
	
	//Puse por mil al producto de G , M1 y M2 porque en el problema dice que est� en cm3/g.seg2
	d= pow(D,2);
	F=(G*M1*M2*1000)/d;
	
	cout<<"La Fuerza gravitacional entre dichos cuerpos es : "<<F;
	
	return 0;
}
