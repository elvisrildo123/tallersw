/**
 * @file Exercise06.cpp
 * @author 	Elvis Chambi (elvisrildo123@gmail.com)
 * @brief 	Dada la hora del d�a en horas, minutos y segundos encuentre la hora del siguiente segundo.
 * @version 2.0
 * @date 28.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalTerminalSeconds = 0;				// Entrada de las horas a trav�s del terminal
int globalMaxSeconds =59;					// Cantidad m�xima de segundos
int globalTerminalMinutes = 0;				// Entrada de los minutos a trav�s del terminal
int globalMaxMinutes = 59;					// Cantidad m�xima de minutos
int globalTerminalHours = 0;				// Entrada de los segundos a trav�s del terminal
int globalMaxHours = 23;					// Cantidad m�xima de horas

int outputseconds = 0;						// Segundos de salida en el terminal
int outputminutes = 0;						// Minutos de salida en el terminal
int outputhours = 0;						// Horas de salida en el terminal

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
int CheckData();
int CheckData2();
void Calculate();
void ShowResults();
int NextHour(int hour, int minute);
int NextMinute(int minute, int second);
int NextSecond(int second);
bool CheckMinutes(int minute);
bool CheckSeconds(int second);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while(true){
		
		if (CollectData() == Error){
			cout<<"\n\t! CollectData Error !\n";
            cout<<"\n\tPlease write a correct data.\n\n";
            continue;
		}
		Calculate();
		ShowResults();
		break;
	}
	
	return true;
}

//=====================================================================================================
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the hour: ";
	globalTerminalHours = CheckData();
	if(globalTerminalHours == -1){
		return Error;
	}
	cout<<"\n\tWrite the minutes: ";
	globalTerminalMinutes = CheckData2();
	if(globalTerminalMinutes == -1){
		return Error;
	}
	cout<<"\n\tWrite the seconds: ";
	globalTerminalSeconds = CheckData2();
	if(globalTerminalSeconds == -1){
		return Error;
	}
	
	return Success;
}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return -1;
			break;	
        } else { 
			if ((data >= 0) && (data <= 23)){ 
			    return data;
				break;
			} else {
				return -1;
			}
		}
    }
}

//=====================================================================================================
int CheckData2(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            cout<<"\n\tIncorrect Data. Please write a whole number: ";
            continue;	
        } else { 
			if ((data >= 0) && (data <= 59)){ 
			    return data;
				break;
			} else {
          	  	cout<<"\n\tIncorrect Data. Please write a correct number (0 - 59): ";			}	
		}
    }
}

//=====================================================================================================
void Calculate(){
	outputseconds = NextSecond(globalTerminalSeconds);
	outputminutes = NextMinute(globalTerminalMinutes,globalTerminalSeconds);
	outputhours = NextHour(globalTerminalHours,globalTerminalMinutes);
}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe time is now: "<<outputhours<<"H:"<<outputminutes<<"M:"<<outputseconds<<"S\r\n";
}

//=====================================================================================================
int NextSecond(int second){
	
	if (CheckSeconds(second)){
		second = 0;
	} else{
		second = second + 1;
	}
	
	return second;
}

//=====================================================================================================
int NextMinute(int minute, int second){
	
	if (CheckSeconds(second)){
		minute = minute +1;
	}
	if (minute > globalMaxMinutes){
		minute = 0;
	}
	return minute;
}

//=====================================================================================================
int NextHour(int hour, int minute){
	
	if (CheckMinutes(minute)){
		hour = hour +1;
	}
	if (hour > globalMaxHours){
		hour = 0;
	}
	return hour;
}

//=====================================================================================================
bool CheckMinutes(int minute){
	
	if (minute == globalMaxMinutes){
		return true;
	} else {
		return false;
	}
}

//=====================================================================================================
bool CheckSeconds(int second){
	
	if (second == globalMaxSeconds){
		return true;
	} else {
		return false;
	}
}

//=====================================================================================================
