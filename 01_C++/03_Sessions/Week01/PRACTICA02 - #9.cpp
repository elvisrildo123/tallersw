/**
 * @file Exercise09.cpp
 * @author 	Elvis Chambi (elvisrildo123@gmail.com)
 * @brief 	Calcular la comisi�n sobre las ventas totales de un empleado, sabiendo que el empleado no recibe comisi�n si su venta es hasta S/.150,
 			si la venta es superior a S/.150 y menor o igual a S/.400 el empleado recibe una comisi�n del 10% de las ventas y si las ventas son mayores a 400,
			entonces la comisi�n es de S/.50 m�s el 9% de las ventas.
 * @version 2.0
 * @date 28.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define COM 50.0								// Comision fija

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalTerminalSells = 0;				// Entrada de la distancia recorrida a trav�s del terminal
int globalMinSells = 150;						// Ventas minimas
int globalMaxSells = 400;						// Ventas m�ximas

float sellerCharges = 0.0;						// Comisiones del vendedor

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
int CheckData();
void Calculate();
void ShowResults();
float CalculateCharges1(int sells);
float CalculateCharges2(int sells);
float TotalCharges(int sells);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while(true){
		
		if (CollectData() == Error){
			cout<<"\n\t! CollectData Error !\n";
            cout<<"\n\tPlease write a correct data.\n\n";
            continue;
		}
		Calculate();
		ShowResults();
		break;
	}
	
	return true;
}

//=====================================================================================================
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the total sells: ";
	globalTerminalSells = CheckData();
	if(globalTerminalSells == -1){
		return Error;
	}
	
	return Success;
}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return -1;
			break;	
        } else { 
			if (data > 0){ 
			    return data;
				break;
			} else {
				return -1;
			}	
		}
    }
}

//=====================================================================================================
void Calculate(){
	sellerCharges = TotalCharges(globalTerminalSells);
}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tTotal of charges: S/."<<sellerCharges<<"\r\n";
}

//=====================================================================================================
float TotalCharges(int sells){
	
	float charges = 0;
	
	if (sells <= globalMinSells){
		charges = 0;
	} else {
		if (sells <= globalMaxSells){
			charges = CalculateCharges1(sells);
		} else {
			charges = CalculateCharges2(sells);
		}
	}

	return charges;
}

//=====================================================================================================
float CalculateCharges1(int sells){
	
	return 0.1 * globalTerminalSells;
}

//=====================================================================================================
float CalculateCharges2(int sells){
	
	return COM + 0.09 * globalTerminalSells;
}

//=====================================================================================================
