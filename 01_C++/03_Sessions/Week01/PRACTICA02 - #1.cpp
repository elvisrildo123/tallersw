/**
 * @file Exercise01.cpp
 * @author Elvis Chambi (elvisrildo123@gmail.com)
 * @brief 	Calcular el pago semanal de un trabajador. Los datos a ingresar son: Total de horas trabajadas y el pago por hora.
			- Si el total de horas trabajadas es mayor a 40 la diferencia se considera 
			como horas extras y se paga un 50% mas que una hora normal.
			- Si el sueldo bruto es mayor a s/. 500.00, se descuenta un 10% en 
			caso contrario el descuento es 0.
 * @version 3.0
 * @date 28.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/


/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalMaxWorkedHours = 40.0; 			// Horas trabajads m�ximas
float globalMaxGrossSalary = 500.0;	 		// Salario bruto m�ximo
bool globalExtraHours = false;				// �Hay horas extras? 
bool globalDiscount = false;				// �Existe descuento?
float globalTerminalWorkedHours = 0.0;		// Entrada de las horas trabajadas a trav�s del terminal
float globalTerminalHourlyPay = 0.0;		// Entrada del pago por hora a trav�s del terminal

float totalGrossSalary = 0.0;				// Sueldo bruto total
float totalNetSalary = 0.0;					// Salario neto

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
int CheckData();
void Calculate();
void ShowResults();
bool CheckWorkedHours(float hours, float maxhours);
float GrossSalary(float hours, float payHour, float maxhours, bool extraHours);
bool CheckGrossSalary(float salary, float maxsalary);
float NetSalary(float grossSalary, bool discount);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while(true){
		
		if (CollectData() == Error){
			cout<<"\n\t! CollectData Error !\n";
            cout<<"\n\tPlease write a correct data.\n\n";
            continue;
		}
		Calculate();
		ShowResults();
		break;
	}
	
	return true;
}

//=====================================================================================================
Result CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the worked hours: ";
	globalTerminalWorkedHours = CheckData();
	if(globalTerminalWorkedHours == -1){
		return Error;
	}
	cout<<"\n\tWrite the hourly pay: S/.";
	globalTerminalHourlyPay = CheckData();
	if(globalTerminalWorkedHours == -1){
	return Error;
	}	
	return Success;
}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return -1;
            break;	
        } else { 
			if (data > 0){ 
			    return data;
				break;
			} else {
          	  	return -1;	
			}	
		}
    }
}

//=====================================================================================================
void Calculate(){
	globalExtraHours = CheckWorkedHours(globalTerminalWorkedHours,globalMaxWorkedHours);
	totalGrossSalary = GrossSalary(globalTerminalWorkedHours,globalTerminalHourlyPay,globalMaxWorkedHours,globalExtraHours);
	globalDiscount = CheckGrossSalary(totalGrossSalary,globalMaxGrossSalary);
	totalNetSalary = NetSalary(totalGrossSalary,globalDiscount);
}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout.precision(4);
	cout<<"\tThe net salary is: "<<"S/."<<totalNetSalary<<"\r\n";
}

//=====================================================================================================
bool CheckWorkedHours(float hours, float maxhours){
	if (hours > maxhours){
		return true;
	} else {
		return false;
	}
}

//=====================================================================================================
float GrossSalary(float hours, float payHour, float maxhours, bool extraHours){
	if (extraHours == true){
		return  1.5 * payHour * (hours - maxhours);
	} else {
		return payHour * hours;
	}
}

//=====================================================================================================
bool CheckGrossSalary(float salary, float maxsalary){
	if (salary > maxsalary){
		return true;
	} else {
		return false;
	}
}

//=====================================================================================================
float NetSalary(float grossSalary, bool discount){
	if (discount == true){
		return 0.9 * grossSalary;
	} else {
		return grossSalary;
	}
}

//=====================================================================================================
