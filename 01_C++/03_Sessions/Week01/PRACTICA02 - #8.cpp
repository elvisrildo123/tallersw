/**
 * @file Exercise08.cpp
 * @author 	Elvis Chambi (elvisrildo123@gmail.com)
 * @brief 	Una empresa registra el sexo, edad y estado civil de sus empleados a trav�s de un n�mero entero positivo de cuatro cifras de acuerdo a lo siguiente:
 			la primera cifra de la izquierda representa el estado civil (1 para soltero, 2 para casado, 3 para viudo y 4 para divorciado),
			las siguientes dos cifras representan la edad y la tercera cifra representa el sexo (1 para femenino y 2 para masculino). 
			Dise�e un programa que determine el estado civil, edad y sexo de un empleado conociendo el n�mero que empaqueta dicha informaci�n.
 * @version 2.0
 * @date 28.02.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalTerminalCode = 0;						// Entrada del codigo de empleado a trav�s del terminal

int globalCivilStatus = 0;						// Estado civil obtenido
int globalAge = 0;								// Edad obtenida
int globalSex = 0;								// Sexo obtenido

/*******************************************************************************************************************************************
 *  												TYPE DEFINITION
 *******************************************************************************************************************************************/
 typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
void CollectData();
int CheckData();
void Calculate();
Result CheckResults();
void ShowResults();
int CalculatedStatusCivil(int code);
int CalculatedAge(int code);
int CalculatedSex(int code);
void StatusCivilMessage();
void SexMessage();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
bool Run(){
	
	while (true){
		
		CollectData();
		Calculate();
		if (CheckResults() == Error){
			cout<<"\n\t! CollectData Error !\n";
			cout<<"\n\tWrong code, out of valid range. Please write a valid code.\n\n";
			continue;
		}
		ShowResults();
		
		break;
	}
	
	return true;
	
}

//=====================================================================================================
void CollectData(){	
	while (true){
		cout<<"========= Insert Data ===========\r\n";
		cout<<"\tWrite worker code: ";
		globalTerminalCode = CheckData();
		if (CheckData == 0){
			continue; 
		}
	
		break;
	}
}

//=====================================================================================================
int CheckData(){
	int data = 0;
	
	while(true){
		cin>>data;
        if(cin.fail() == true){
        	cin.clear();
            cin.ignore(1000,'\n');
            return 0;
            break;
        } else { 
			if (data >= 1181 && data <= 4652){ 
			    return data;
				break;
			} else {
          	  	return 0;
			}	
		}
    }
}

//=====================================================================================================
void Calculate(){
	globalCivilStatus = CalculatedStatusCivil(globalTerminalCode);
	globalSex = CalculatedSex(globalTerminalCode);
	globalAge = CalculatedAge(globalTerminalCode);
}

//=====================================================================================================
Result CheckResults(){

	if (globalAge < 18 || globalAge > 65){
		return Error;
	} else {
		if (globalSex == 1 || globalSex == 2){
			return Success;
		} else {
			return Error;
		}
	}
}

//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	StatusCivilMessage();
	cout<<"\n\tAge: "<<globalAge<<"\r\n";
	SexMessage();
}

//=====================================================================================================
int CalculatedStatusCivil(int code){
	return code / 1000;
}

//=====================================================================================================
int CalculatedAge(int code){
	return (code % 1000 - globalSex)/10;
}

//=====================================================================================================
int CalculatedSex(int code){
	return code%10;
}

//=====================================================================================================
void StatusCivilMessage(){
	
	switch(globalCivilStatus){
		
		
		case 1:
			cout<<"\tCivil Status: Single";	
			break;
		case 2:
			cout<<"\tCivil Status: Married";
			break;
		case 3:
			cout<<"\tCivil Status: Widower";
			break;	
		case 4:
			cout<<"\tCivil Status: Divorced";
			break;
		default:
			cout<<"INCORRECT CODE";
			break;
	}
}

//=====================================================================================================

void SexMessage(){
	
	switch(globalSex){
		case 1:
			cout<<"\tSex: Female";
			break;
		case 2:
			cout<<"\tSex: Male";
			break;
		default:
			cout<<"INCORRECT CODE";
			break;
	}
}
