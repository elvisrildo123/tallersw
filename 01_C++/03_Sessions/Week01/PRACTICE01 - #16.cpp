/*
16. Escribe, compila y ejecuta un programa en C ++ que calcule y devuelva la ra�z cuarta de un n�mero.
 Pru�balo con el n�mero 81.0 (deber� devolverte 3). Utiliza el programa para calcular la ra�z cuarta de 1728.8964
 */
 
 #include <iostream>
 #include <math.h>
 
 using namespace std;
 
 double numberInput=0.0;
 double rootInput=0.0;
 double result=0.0;
 
void Run();
void CollectData();
void Calculate();
void ShowResults();
double calculationRoot(double number, double root);

 int main(){
	
	Run();
 	return 0;
 	
 }

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

void CollectData(){	
 	cout<<"============Insert data============\r\n";
 	cout<<"Calculation of any root\r\n";
 	cout<<"\tEnter the number: ";
 	cin>>numberInput;
 	cout<<"\tEnter the root: ";
 	cin>>rootInput;

}


void Calculate(){
	
	result=calculationRoot(numberInput, rootInput); 
 	
}

void ShowResults(){
 	cout<<"============Show result============\r\n";
 	cout<<"The root of the number is:"<<result<<"\r\n";
 

}

double calculationRoot(double number, double root){
	
	return pow(number, 1.0/root);
	
}






