/**
 * @file Condicionales.cpp
 * @author Elvis Chambi (elvisrildo123@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 12.02.2022
 * 
 * && y
 * || o 
 * == igual
 * != diferente
 *
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include<iostream>
#include<stdlib.h>
#include<math.h>
#define PI 3.14

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

float radio = 0;
float altura = 0;
char opc = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

float mostrar(float radio, float altura, char opc);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
	
int main(){
	
	
	cout<<"Ingrese radio: "; 
	cin>>radio;
	cout<<"Ingrese altura: "; 
	cin>>altura;
	
	cout<<"\nElija una opcion :"<<endl;
	cout<<" a. Volumen de un cubo"<<endl;
	cout<<" b. Volumen de un cilindro"<<endl;
	cout<<" c. Volumen de un esfera"<<endl;
	cin>>opc;
	
	mostrar(radio,altura,opc);
	
	system("pause");
	return 0;	
}

//=====================================================================================================


float mostrar(float radio, float altura, char opc){
	float area, volumen;
	
	switch(opc){
		case 'a':
			volumen = radio*radio*radio;
			cout<<"El volumen es : "<<volumen<<endl;
			break;
		case 'b':
			volumen = radio*PI*radio*altura;
			cout<<"El volumen es : "<<volumen<<endl;
			break;
		case 'c':
			volumen = 4/3*PI*radio*radio*radio;
			cout<<"El volumen es : "<<volumen<<endl;
			break;
		default: break; 
	}
}
