
/*****************************************************************************INCLUDE**********************************************************************************************/
#include <iostream>
using namespace std;
/*
Exercise 3
*/
//******************************************************************FUNCTION DECLARATION************************************************************************************************
void Run();
void CollectData();
void Calculate();
void ShowResults(); 
/*******************************************************************MAIN*****************************************************************************************************************/
int main() {
	Run();
	return 0;
	}
//******************************************************************************GLOBAL VARIABLE*************************************************************************************************	
	int aprobados;
	int desaprobados;
	int notables;
	float p_desaprobados;
	float p_notables;
	float p_sobresalientes;
	float sobresalientes;
	float superan;
	float total;
//********************************************************************FUNCTION DEFINITION*************************************************************************************************
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//===================================================================================================================================================================================
void CollectData(){

	cout << "Ingresar cantidad de alumnos sobresalientes" << endl;
	cin >> sobresalientes;
	cout << "Ingresar cantidad de alumnos notables" << endl;
	cin >> notables;
	do {
		cout << "Ingresar cantidad de alumnos aprobados" << endl;
		cin >> aprobados;
	} while ((aprobados<notables && aprobados<sobresalientes));
	cout << "Ingresar cantidad de alumnos reprobados" << endl;
	cin >> desaprobados;
}
//======================================================================================================================================================================================================================
void Calculate(){
	total = (aprobados+desaprobados);
	superan = (aprobados/total)*100;
	p_notables = (notables/total)*100;
	p_sobresalientes = (sobresalientes/total)*100;
	p_desaprobados = (desaprobados/total)*100;
}
//=======================================================================================================================================================================================
void ShowResults(){
	cout << "El " << superan << "% superaron la materia" << endl;
	cout << "El " << p_desaprobados << "% reprobaron la materia" << endl;
	cout << "El " << p_notables << "% tienen notas notables" << endl;
	cout << "El " << p_sobresalientes << "% tienen notas sobresalientes" << endl;
}

